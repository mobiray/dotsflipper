﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using GooglePlayGames;
using GooglePlayGames.BasicApi;
using UnityEngine.SocialPlatforms;
using UnityEngine.UI;

public class GPlayManager : MonoBehaviour {

    //public Text ballsCountText;
    //public Text touchesCountText;
    public int ballsOnFieldCount { get; protected set; }
    public int touchesByOneHit { get; protected set; }

    static public GPlayManager Instance { get { return _instance; } }
    static protected GPlayManager _instance;

    public bool isAuthenticate;
    void Awake()
    {
        _instance = this;
        PlayGamesPlatform.Activate();
        Social.localUser.Authenticate((bool success) => { isAuthenticate = success; });
    }

    void Start()
    {
        ballsOnFieldCount = 0;
    }

    //public void AppendBallsCount()
    //{
    //    ballsOnFieldCount++;
    //    //ballsCountText.text = "ballsOnField_" + ballsOnFieldCount + "_";
    //    if(ballsOnFieldCount >= 5)
    //    {
    //        Social.localUser.Authenticate((bool success) => {
    //            // handle success or failure
    //        });
    //        Social.ReportProgress("CgkIs9KWt4YHEAIQAw", 100.0f, (bool success) => {
    //            // handle success or failure
    //        });
    //        if (ballsOnFieldCount >= 10)
    //        {
    //            Social.ReportProgress("CgkIs9KWt4YHEAIQBA", 100.0f, (bool success) => {
    //                // handle success or failure
    //            });
    //        }
    //    }

    //}

    //public void DeductBallsCount()
    //{
    //    ballsOnFieldCount--;
    //   // ballsCountText.text = "ballsOnField_" + ballsOnFieldCount + "_";
    //}

    //public void UnnihilateBallsCount()
    //{
    //    ballsOnFieldCount = 0;
    //    //ballsCountText.text = "ballsOnField_" + ballsOnFieldCount + "_";
    //}

    //public void AppendTouchesCount()
    //{
    //    touchesByOneHit++;
    //   // touchesCountText.text = "ballsOnField_" + touchesByOneHit + "_";
    //    if (touchesByOneHit >= 3)
    //    {
    //        Social.localUser.Authenticate((bool success) => {
    //            // handle success or failure
    //        });
    //        Social.ReportProgress("CgkIs9KWt4YHEAIQBQ", 100.0f, (bool success) => {
    //            // handle success or failure
    //        });
    //        if (touchesByOneHit >= 4)
    //        {
    //            Social.ReportProgress("CgkIs9KWt4YHEAIQBg", 100.0f, (bool success) => {
    //                // handle success or failure
    //            });
    //        }
    //    }

    //}

    //public void UnnihilateTouchesCount()
    //{
    //    touchesByOneHit = 0;
    //   // touchesCountText.text = "ballsOnField_" + touchesByOneHit + "_";
    //}

    //public void CheckScore(int score)
    //{
    //    if(score >= 10)
    //    {
    //        Social.localUser.Authenticate((bool success) => {
    //            // handle success or failure
    //        });
    //        Social.ReportProgress("CgkIs9KWt4YHEAIQAQ", 100.0f, (bool success) => {
    //            // handle success or failure
    //        });
    //        if (score >= 100)
    //        {
    //            Social.ReportProgress("CgkIs9KWt4YHEAIQAg", 100.0f, (bool success) => {
    //                // handle success or failure
    //            });
    //        }
    //    }
    //}

    public void ReportScore(int score)
    {
//        Social.localUser.Authenticate((bool success) => {
//            // handle success or failure
//        });
        if (isAuthenticate)
        {
            Social.ReportScore(score, "CgkIs9KWt4YHEAIQBw", (bool success) => {
                // handle success or failure
            });
        }
    }
}
