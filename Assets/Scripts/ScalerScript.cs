﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScalerScript : MonoBehaviour {

    void Awake()
    {
        RectTransform rt = this.GetComponent<RectTransform>();
        float screenSizeY = 1080 * Screen.height / Screen.width;
        rt.sizeDelta = new Vector2(1082, screenSizeY+2);
        rt.anchoredPosition = new Vector2(0, 0);
    }
}
