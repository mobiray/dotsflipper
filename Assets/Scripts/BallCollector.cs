﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BallCollector : MonoBehaviour {

    GameManager gMan;
    LevelManager lMan;
    void Start()
    {
        gMan = GameManager.Instance;
        lMan = LevelManager.Instance;
    }
    void OnTriggerEnter2D(Collider2D coll)
    {
        if (coll.CompareTag("Player"))
        {
            Destroy(coll.gameObject);
            gMan.DeductLive();
            if(gMan.currentLives == 0)
            {
                //gMan.RestartStage();
                GUIManager.Instance.SetDefeatScreen(true);
            } else
            {
                gMan.Initialize();
            }
        }
        if (coll.CompareTag("Collection"))
        {
           // gpMan.DeductBallsCount();
            coll.GetComponent<PoolableObject>().Destroy();
        }
    }


}
