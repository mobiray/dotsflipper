﻿using UnityEngine;
using System.Collections;
using System;

public class PoolableObject : MonoBehaviour 
{

	public virtual void Destroy()
	{
        gameObject.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Kinematic;
		gameObject.SetActive(false);
	}

}
