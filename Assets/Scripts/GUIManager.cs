﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class GUIManager : MonoBehaviour {

    public GameObject PauseScreen;
    public GameObject DefeatScreen;

    public Text CompleteText;
    public Text LivesText;
    public Text ScoreText;
    public RectTransform[] PauseScreenRects;

    public RectTransform ContinueButton;
    public RectTransform RestartButton;

    private Image PauseScreenImage;
    private Image DefeatScreenImage;

    private float screenSizeY;

    static public GUIManager Instance { get { return _instance; } }
    static protected GUIManager _instance;
    void Awake()
    {
        _instance = this;
        if(PlayerPrefs.GetInt("Sound", 1) == 0)
        {
            PauseScreenRects[5].GetComponent<Toggle>().isOn = false;
        }
    }

    void Start()
    {
        PauseScreenImage = PauseScreen.GetComponent<Image>();
        DefeatScreenImage = DefeatScreen.GetComponent<Image>();
    }
    public void SetCompleteText(string text)
    {
        CompleteText.text = text;
        CompleteText.DOFade(1f, 2f).SetEase(Ease.Linear);
        CompleteText.DOFade(0f, 1f).SetEase(Ease.Linear).SetDelay(2.1f);
    }

    public void SetLivesText(string text)
    {
        LivesText.text = text;
    }

    public void SetScoreText(string text)
    {
        ScoreText.text = "SCORE:  " + text;
    }

    public void SetPauseScreen(bool state)
    {
        PauseScreenImage.raycastTarget = state;
        if (state)
        {
            PauseScreen.SetActive(true);
            Time.timeScale = 0;
//            PauseScreenImage.DOFade(0.7f, 0.7f).SetUpdate(true);
            PauseScreen.GetComponent<CanvasGroup>().DOFade(1f, 0.6f).SetUpdate(true);
            /*TODO: Rewrite this part*/
            PauseScreenRects[0].DOAnchorPos(new Vector2(-90f, 90f), 0.5f).SetUpdate(true);
            PauseScreenRects[1].DOAnchorPos(new Vector2(-20f, 670f), 0.5f).SetUpdate(true);
            PauseScreenRects[2].DOAnchorPos(new Vector2(-270f, 645f), 0.5f).SetUpdate(true);
            PauseScreenRects[3].DOAnchorPos(new Vector2(-500f, 500f), 0.5f).SetUpdate(true);
            PauseScreenRects[4].DOAnchorPos(new Vector2(-645f, 270f), 0.5f).SetUpdate(true);
            PauseScreenRects[5].DOAnchorPos(new Vector2(-660f, 20f), 0.5f).SetUpdate(true);
            for(int i=0; i<5; i++)
            {
                PauseScreenRects[i].GetComponent<Image>().DOFade(1f, 0.25f).SetUpdate(true);
            }
            PauseScreenRects[5].GetChild(0).GetComponent<Image>().DOFade(1f, 0.25f).SetUpdate(true);
            PauseScreenRects[5].GetChild(0).GetChild(0).GetComponent<Image>().DOFade(1f, 0.25f).SetUpdate(true);

            //coordY = 0;
        } else
        {
            Time.timeScale = 1;
            StartCoroutine(DeactivatePauseScreenCo());
//            PauseScreenImage.DOFade(0.0f, 0.5f);
            PauseScreen.GetComponent<CanvasGroup>().DOFade(0f, 0.4f).SetUpdate(true);
            PauseScreenRects[0].DOAnchorPos(new Vector2(600f, -600f), 0.25f).SetUpdate(true);
            PauseScreenRects[1].DOAnchorPos(new Vector2(-61f, 2080f), 0.25f).SetUpdate(true);
            PauseScreenRects[2].DOAnchorPos(new Vector2(-804f, 1920f), 0.25f).SetUpdate(true);
            PauseScreenRects[3].DOAnchorPos(new Vector2(-1471f, 1471f), 0.25f).SetUpdate(true);
            PauseScreenRects[4].DOAnchorPos(new Vector2(-1920f, 804f), 0.25f).SetUpdate(true);
            PauseScreenRects[5].DOAnchorPos(new Vector2(-2080f, 61f), 0.25f).SetUpdate(true);
            for (int i = 0; i < 5; i++)
            {
                PauseScreenRects[i].GetComponent<Image>().DOFade(0f, 0.10f).SetUpdate(true);
            }
            PauseScreenRects[5].GetChild(0).GetComponent<Image>().DOFade(0f, 0.10f).SetUpdate(true);
            PauseScreenRects[5].GetChild(0).GetChild(0).GetComponent<Image>().DOFade(0f, 0.10f).SetUpdate(true);
            //coordY = 1080 * Screen.height / Screen.width;
        }

        //PauseScreen.GetComponent<RectTransform>().DOAnchorPosY(coordY, 1.5f).SetUpdate(true).SetEase(Ease.OutQuint);
    }

    public IEnumerator DeactivatePauseScreenCo()
    {
        yield return new WaitForSeconds(0.5f);
        PauseScreen.SetActive(false);
    }

    public void SetDefeatScreen(bool state)
    {
        
        DefeatScreenImage.raycastTarget = state;
       
        if (state)
        {
            DefeatScreen.SetActive(true);
            SetActiveDefeatButtons();
            if (GameManager.Instance.isAdsShowed || !AdvertismentScript.Instance.isRewardedVideoLoaded())
            {
                RestartButton.sizeDelta = new Vector2(1080f, 960f);
            }
            else
            {
                RestartButton.sizeDelta = new Vector2(540f, 960f);
                ContinueButton.DOAnchorPosY(0f, 0.75f).SetEase(Ease.OutBack).SetUpdate(true);
            }
            Time.timeScale = 0;
            DefeatScreenImage.DOFade(0.75f, 0.7f).SetUpdate(true);
            RestartButton.DOAnchorPosY(0f, 0.75f).SetEase(Ease.OutBack).SetUpdate(true);
        } else
        {
            Time.timeScale = 1;
            StartCoroutine(DeactivateDefeatScreenCo());
            DefeatScreenImage.DOFade(0f, 0.5f).SetUpdate(true);
            ContinueButton.DOAnchorPosY(-960f, 0.25f).SetUpdate(true);
            RestartButton.DOAnchorPosY(-960f, 0.25f).SetUpdate(true);
        }
    }

    public IEnumerator DeactivateDefeatScreenCo()
    {
        yield return new WaitForSeconds(0.5f);
        DefeatScreen.SetActive(false);
    }

    public void SetInactiveWatchButton()
    {
        ContinueButton.GetComponent<Button>().interactable = false;
    }
    public void SetInactiveRestartButton()
    {
        RestartButton.GetComponent<Button>().interactable = false;
    }
    public void SetActiveDefeatButtons()
    {
        ContinueButton.GetComponent<Button>().interactable = true;
        RestartButton.GetComponent<Button>().interactable = true;
    }
}
