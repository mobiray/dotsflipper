﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class SpecButtonsTransitions : MonoBehaviour
{
    public float Delay = 1f;
    public float FinalPosX = 15f;

    private float _initPosX;
    private RectTransform _thisRT;

    private void Awake()
    {
        _thisRT = GetComponent<RectTransform>();
        _initPosX = _thisRT.anchoredPosition.x;
    }

    private void OnEnable()
    {
        DOTween.Kill(_thisRT);
//        _thisRT.anchoredPosition = new Vector2(FinalPosX, _thisRT.anchoredPosition.y);
        _thisRT.DOAnchorPos(new Vector2(FinalPosX, _thisRT.anchoredPosition.y), 0.5f).SetUpdate(true);
//        Debug.Log("OnEnable " + transform.name);
    }

    private void OnDisable()
    {
        DOTween.Kill(_thisRT);
        _thisRT.anchoredPosition =
            new Vector2(_initPosX, _thisRT.anchoredPosition.y);
    }

    public void OnPrivacyPolicyButtonClick()
    {
        Application.OpenURL("https://2d2b.github.io/privacy/com.twodtwob.relaxdots.html");
    }
}