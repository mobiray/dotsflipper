﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class GameManager : MonoBehaviour {

    public GameObject ballPrefab;
    [HideInInspector]
    public GameObject ball;

    public bool isFirstTouch = true;

    public int ballsToKnock = 0;
    public int totalLives { get; protected set; }
    public int currentLives { get; protected set; }
    public int score { get; protected set; }
    public bool isAdsShowed { get; protected set; }
    public float interstitialTime;

    private bool isFirstRun = true;

    static public GameManager Instance { get { return _instance; } }
    static protected GameManager _instance;
    void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else if (_instance != this)
        {
            Destroy(gameObject);
        }
        
        SetInitialData();
        Initialize();
        interstitialTime = Time.time + 30f;
      
        if (PlayerPrefs.GetInt("IsFirstRun", 0) == 0)
        {
            PlayerPrefs.SetInt("IsFirstRun", 1);
            interstitialTime = Time.time + 270f;
        }
    }

    private void SetInitialData()
    {
        score = PlayerPrefs.GetInt("Score", 0);
        totalLives = 3;
        currentLives = totalLives;
    }

    public void Initialize()
    {
        GUIManager.Instance.SetLivesText(Convert.ToString(currentLives));
        ball = Instantiate(ballPrefab);
        ball.transform.position = new Vector3(-1.5f, -1.5f, 0.0f);
        //ball.transform.position = new Vector3(0.271397f, 2.8f, 0.0f);

        isFirstTouch = true;
        if(Time.time >= interstitialTime && !isFirstRun && AdvertismentScript.Instance.isInterstitialLoaded())
        {
            AdvertismentScript.Instance.ShowInterstitial();
            interstitialTime = Time.time + 180f;
        }
        isFirstRun = false;
    }

    public void SetScore(int newScore)
    {
        score = newScore;
        PlayerPrefs.SetInt("Score", score);
    }

    public void RestartStage()
    {
        isAdsShowed = false;
        //if(currentLives == 0)
        //{
        //    currentLives = totalLives;
        //    LevelManager.Instance.LoadStage();
        //}
        if(ball != null)
        {
            Destroy(ball.gameObject);
        }
        currentLives = totalLives;
        LevelManager.Instance.LoadStage();
        Initialize();
    }

    public void CompleteStage()
    {
        isAdsShowed = false;
        int lvl = PlayerPrefs.GetInt("CurrentLevel");
        PlayerPrefs.SetInt("CurrentLevel", lvl + 1);
        SetScore(LevelManager.Instance.currentScore);
        //*HERE*//
        GPlayManager.Instance.ReportScore(LevelManager.Instance.currentScore);
        Destroy(ball.gameObject);
        currentLives = totalLives;
        LevelManager.Instance.LoadStage();
        Initialize();
    }

    public void DecompleteStage()
    {
        int lvl = PlayerPrefs.GetInt("CurrentLevel");
        PlayerPrefs.SetInt("CurrentLevel", lvl - 1);
        SetScore(LevelManager.Instance.currentScore);
        Destroy(ball.gameObject);
        currentLives = totalLives;
        LevelManager.Instance.LoadStage();
        Initialize();
    }

    public void DeductLive()
    {
        currentLives--;
    }

    public void AppendLive()
    {
        isAdsShowed = true;
        currentLives++;
    }
    //SceneManager.LoadScene(SceneManager.GetActiveScene().name);

    void OnApplicationQuit()
    {
        PlayerPrefs.DeleteAll();
    }
}
