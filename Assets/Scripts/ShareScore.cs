﻿using UnityEngine;
using System.Collections;

public class ShareScore : MonoBehaviour {

	public void OnShareButton()
    {
        //instantiate the class Intent
        AndroidJavaClass intentClass = new AndroidJavaClass("android.content.Intent");

        //instantiate the object Intent
        AndroidJavaObject intentObject = new AndroidJavaObject("android.content.Intent");

        //call setAction setting ACTION_SEND as parameter
        intentObject.Call<AndroidJavaObject>("setAction", intentClass.GetStatic<string>("ACTION_SEND"));

        //instantiate the class Uri
        AndroidJavaClass uriClass = new AndroidJavaClass("android.net.Uri");

        //instantiate the object Uri with the parse of the url's file
        //   AndroidJavaObject uriObject = uriClass.CallStatic<AndroidJavaObject>("parse", "Sharing");

        //call putExtra with the uri object of the file
        intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_TEXT"), "I'm on the level " + (PlayerPrefs.GetInt("CurrentLevel") + 1) + " in Dots Flipper game "+ "https://play.google.com/store/apps/details?id=com.twodtwob.relaxdots");

        //set the type of file
        intentObject.Call<AndroidJavaObject>("setType", "text/plain");

        //instantiate the class UnityPlayer
        AndroidJavaClass unity = new AndroidJavaClass("com.unity3d.player.UnityPlayer");

        //instantiate the object currentActivity
        AndroidJavaObject currentActivity = unity.GetStatic<AndroidJavaObject>("currentActivity");

        //call the activity with our Intent
        currentActivity.Call("startActivity", intentObject);
    }
}
