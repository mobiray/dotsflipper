﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class TapController : MonoBehaviour, IPointerDownHandler
{

    public Rigidbody2D rb;

    public void OnPointerDown(PointerEventData eventData)
    {
        if(!GameManager.Instance.isFirstTouch)
        {
            AudioController.Play("ArmSound");
            rb.AddForce(new Vector2(0.0f, 2000.0f));
        }
        else
        {
            GameManager.Instance.ball.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Dynamic;
            GameManager.Instance.isFirstTouch = false;
            //GUIManager.Instance.SetCompleteText("");
        }
    }

}
