﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelStorage : MonoBehaviour {

    /*TODO: make all fields private*/
    //[Serializable]
    //public class Level
    //{
    //    [SerializeField]
    //    public List<string> colors;
    //    [SerializeField]
    //    public List<string> map; 
    //}

    [Serializable]
    public class CScheme
    {
        [SerializeField]
        public string scheme;
    }
    [Serializable]
    public class CSchemes
    {
        [SerializeField]
        public List<CScheme> schemes = new List<CScheme>();
    }

    [Serializable]
    public class MapPattern
    {
        [SerializeField]
        public List<string> pattern = new List<string>();
        [SerializeField]
        public string appearenceSequence;
    }
    [Serializable]
    public class MapPatterns
    {
        [SerializeField]
        public List<MapPattern> patterns = new List<MapPattern>();
    }


    //[Serializable]
    //public class Levels
    //{
    //    [SerializeField]
    //    public List<Level> lvls = new List<Level>();
    //}

    //public Levels LevelsList { get; protected set; }
    public CSchemes colorSchemes { get; protected set; }
    public MapPatterns mapPatterns { get; protected set; }


    static public LevelStorage Instance { get { return _instance; } }
    static protected LevelStorage _instance;
    void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else if (_instance != this)
        {
            Destroy(gameObject);
        }
        string tmpCShString = Resources.Load<TextAsset>("ColorSchemes").text;
        colorSchemes = JsonUtility.FromJson<CSchemes>(tmpCShString);
        string tmpMPString = Resources.Load<TextAsset>("MapPatterns").text;
        mapPatterns = JsonUtility.FromJson<MapPatterns>(tmpMPString);
    }

    void Start()
    {

        //MapPatterns cSh = new MapPatterns();
        //MapPattern p1 = new MapPattern();
        //p1.pattern.Add("asd");
        //p1.pattern.Add("asd2");
        //p1.appearenceSequence = "123";
        //MapPattern p2 = new MapPattern();
        //p2.pattern.Add("asd3");
        //p2.pattern.Add("asd4");
        //p1.appearenceSequence = "34567";
        //cSh.patterns.Add(p1);
        //cSh.patterns.Add(p2);
        //string tmpJson = JsonUtility.ToJson(cSh);
        //Debug.Log(tmpJson);
        //CScheme clrScheme = new CScheme();
        //clrScheme.scheme = "asd asd Asd";
        //CSchemes cShs = new CSchemes();
        //cShs.schemes.Add(clrScheme);
        //cShs.schemes.Add(clrScheme);
        //string tmpStringd = JsonUtility.ToJson(cShs);
        //Debug.Log(tmpStringd);
        //Levels lvls = new Levels();
        //Level lvl = new Level();
        //lvl.rowsCount = 3;
        //lvl.colorsCount = 3;
        //lvl.colors = new List<string>();
        //lvl.colors.Add("0.88 1.0 0.63");
        //lvl.colors.Add("1.0 0.27 0.82");
        //lvl.colors.Add("0.33 0.73 1.0");

        //lvl.map = new List<string>();
        //for (int i = 0; i < lvl.rowsCount; i++)
        //{
        //    if (i == 1)
        //    {
        //        lvl.map.Add("1 1 1 1 1 1 1");
        //    }
        //    else
        //    {
        //        lvl.map.Add("1 1 1 1 1 1 1 1");
        //    }
        //}
        //Level lvl2 = new Level();
        //lvl2.rowsCount = 3;
        //lvl2.colorsCount = 3;
        //lvl2.colors = new List<string>();
        //lvl2.colors.Add("0.33 0.73 1.0");
        //lvl2.colors.Add("1.0 0.27 0.82");
        //lvl2.colors.Add("0.88 1.0 0.63");

        //lvl2.map = new List<string>();
        //for (int i = 0; i < lvl.rowsCount; i++)
        //{
        //    if (i == 1)
        //    {
        //        lvl2.map.Add("2 2 2 2 2 2 2");
        //    }
        //    else
        //    {
        //        lvl2.map.Add("2 2 2 2 2 2 2 2");
        //    }
        //}
        //lvls.lvls.Add(lvl);
        //lvls.lvls.Add(lvl2);
        //string json_string = JsonUtility.ToJson(lvls);
    }
}
