﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using DG.Tweening;

public class LevelManager : MonoBehaviour {


    public GameObject ballPrefab;
    public int maxBallsInRow = 9;

    private int currentLevel;
   // private LevelStorage.Level stage;
    private float topBound;
    private float rightBound;
    private float gap;
    private float ballRadius;
    private float offset;
    public int currentScore { get; protected set; }

    public ObjectPooler _objectPooler { get; protected set; }
    static public LevelManager Instance { get { return _instance; } }
    static protected LevelManager _instance;
    void Awake()
    {
        _instance = this;
        _objectPooler = this.GetComponent<ObjectPooler>();
    }
    void Start()
    {
        Camera cam = FindObjectOfType<Camera>().GetComponent<Camera>();
        topBound = cam.orthographicSize;
        rightBound = topBound * Screen.width / Screen.height;
        ballRadius = ballPrefab.GetComponent<CircleCollider2D>().radius;
        offset = 1200 * Screen.width / (1080f * Screen.height);
        //  gap = Mathf.Min((2 * rightBound - 2 * maxBallsInRow * ballRadius) / (maxBallsInRow + 1), 0.17f);
        gap =(2 * rightBound - 2 * maxBallsInRow * ballRadius) / (maxBallsInRow + 2);
        offset += 1.5f * gap;
        LoadStage();
    }

    public void IncrementCurrentScore()
    {
        currentScore++;
       // gpManager.CheckScore(currentScore);
        GUIManager.Instance.SetScoreText(Convert.ToString(currentScore));
    }

    public void LoadStage()
    {
    //    gpManager.UnnihilateBallsCount();
        currentScore = GameManager.Instance.score;
        GUIManager.Instance.SetScoreText(Convert.ToString(currentScore));
        ClearGameField();
        currentLevel = PlayerPrefs.GetInt("CurrentLevel", 0);
        GUIManager.Instance.SetCompleteText("Level " + Convert.ToString(currentLevel + 1));
        int patternCount = LevelStorage.Instance.mapPatterns.patterns.Count;
        int currentColor = currentLevel + (int)(currentLevel / patternCount);
        currentColor %= LevelStorage.Instance.colorSchemes.schemes.Count;
        //Debug.Log("Current COLOR SCHEME = " + currentColor);
        currentLevel %= patternCount;
        //Debug.Log("Current LEVEL = " + currentLevel);
        
        LevelStorage.MapPattern map = LevelStorage.Instance.mapPatterns.patterns[currentLevel];
        LevelStorage.CScheme cSheme = LevelStorage.Instance.colorSchemes.schemes[currentColor];
        GameManager.Instance.ballsToKnock = 0;
        int rowsCount = map.pattern.Count;
        string[] clrs = cSheme.scheme.Split(' ');
        string[] sequence = map.appearenceSequence.Split(' ');
        int colorsCount = clrs.Length;
        int[][] stageMap = new int[rowsCount][];
        for (int i = 0; i < rowsCount; i++)
        {
            if (map.pattern[i] != null)
            {
                string[] tmpStr = map.pattern[i].Split(' ');
                stageMap[i] = new int[tmpStr.Length];
                for (int j = 0; j < tmpStr.Length; j++)
                {
                    stageMap[i][j] = int.Parse(tmpStr[j]);
                }
            }
        }
        Color[] colorMap = new Color[colorsCount];
        for (int i = 0; i < colorsCount; i++)
        {
            ColorUtility.TryParseHtmlString(("#" + clrs[i]), out colorMap[i]);
        }
        ArrayList appSequence = new ArrayList();
        for(int i=0; i<sequence.Length; i++)
        {
            appSequence.Add(int.Parse(sequence[i]));
        }
        float delay = 0f;
        for (int i = 0; i < stageMap.GetLength(0); i++)
        {
            int correction = (stageMap[i].GetLength(0) + 1) % 2;
            for (int j = 0; j < stageMap[i].GetLength(0); j++)
            {
                int character = stageMap[i][j];
                if (character != 0)
                {
                    GameManager.Instance.ballsToKnock++;
                    GameObject ball = _objectPooler.GetPooledGameObject();
                    //GameObject ball = Instantiate(ballPrefab);
                    //ball.transform.SetParent(this.transform);
                    if (ball.activeSelf)
                    {
                        ball.SetActive(false);
                    }
                    ball.transform.position = new Vector3((j - (stageMap[i].GetLength(0) - 1) / 2f) * (2 * ballRadius + gap), topBound - (2 * i + 1) * (ballRadius + gap / 4) - offset, 0.0f);
                    ball.GetComponent<SpriteRenderer>().color = new Color(colorMap[character - 1].r, colorMap[character - 1].g, colorMap[character - 1].b, 0f);
                    ball.SetActive(true);
                    delay = 0.1f * appSequence.IndexOf(character);
                    ball.transform.Find("Shader").GetComponent<SpriteRenderer>().DOFade(0.2f, 1.2f).SetEase(Ease.OutBounce).SetDelay(delay + 0.1f);
                    ball.GetComponent<SpriteRenderer>().DOFade(1.0f, 0.75f).SetEase(Ease.InCubic).SetDelay(delay);
                }

            }
        }
    }

    public void ClearGameField()
    {
        this.GetComponent<SimpleObjectPooler>().ClearPool();
    }
}
