﻿using UnityEngine;
using System.Collections;
using AppodealAds.Unity.Api;
using AppodealAds.Unity.Common;
using UnityEngine.UI;
using System;

public class AdvertismentScript : MonoBehaviour, IRewardedVideoAdListener
{
    static public AdvertismentScript Instance
    {
        get { return _instance; }
    }

    static protected AdvertismentScript _instance;

    private string appKey = "88569b4bfffd77db89029f31b7f27238c03d5b95b0533cc0";

    void Start()
    {
        if (_instance == null)
        {
            _instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else if (_instance != this)
        {
            Destroy(gameObject);
        }

        Appodeal.disableLocationPermissionCheck();
        Appodeal.disableNetwork("ogury");

        Init();
    }

    private bool isInitialized = false;

    public void Init()
    {
        if (!isInitialized)
        {
            if (PlayerPrefs.HasKey("result_gdpr"))
            {
                if (PlayerPrefs.GetInt("result_gdpr") == 1)
                {
                    Appodeal.initialize(appKey, Appodeal.INTERSTITIAL | Appodeal.REWARDED_VIDEO, true);
                    Appodeal.setRewardedVideoCallbacks(this);
                    isInitialized = true;
                }
                else
                {
                    Appodeal.initialize(appKey, Appodeal.INTERSTITIAL | Appodeal.REWARDED_VIDEO, false);
                    Appodeal.setRewardedVideoCallbacks(this);
                    isInitialized = true;
                }
            }
            else if (PlayerPrefs.GetInt("isNotNeedtoShowGPDR") == 1)
            {
                Appodeal.initialize(appKey, Appodeal.INTERSTITIAL | Appodeal.REWARDED_VIDEO, true);
                Appodeal.setRewardedVideoCallbacks(this);
                isInitialized = true;
            }

        }
        else
        {
            Debug.Log("appodealinit fe");
        }
    }

    public void ShowVideo()
    {
        Appodeal.show(Appodeal.REWARDED_VIDEO);
    }

    public bool isRewardedVideoLoaded()
    {
        return Appodeal.isLoaded(Appodeal.REWARDED_VIDEO);
    }

    public void ShowInterstitial()
    {
        Appodeal.show(Appodeal.INTERSTITIAL);
    }

    public bool isInterstitialLoaded()
    {
        return Appodeal.isLoaded(Appodeal.INTERSTITIAL);
    }

    public void onRewardedVideoLoaded(bool precache)
    {
    }

    public void onRewardedVideoFailedToLoad()
    {
    }

    public void onRewardedVideoShown()
    {
        GUIManager.Instance.SetActiveDefeatButtons();
    }

    public void onRewardedVideoFinished(double amount, string name)
    {
        GameManager.Instance.interstitialTime += 40f;
        GameManager.Instance.AppendLive();
        GameManager.Instance.Initialize();
        GUIManager.Instance.SetDefeatScreen(false);
    }

    public void onRewardedVideoClosed(bool finished)
    {
        GUIManager.Instance.SetActiveDefeatButtons();
    }

    public void onRewardedVideoExpired()
    {
    }

    //public void onNonSkippableVideoLoaded()
    //{
    //    GUIManager.Instance.PlusAdsText();
    //}

    //public void onNonSkippableVideoFailedToLoad()
    //{
    //    GUIManager.Instance.SetActiveDefeatButtons();
    //    GUIManager.Instance.MinusAdsText();
    //}

    //public void onNonSkippableVideoShown()
    //{
    //    GUIManager.Instance.SetActiveDefeatButtons();
    //}

    //public void onNonSkippableVideoFinished()
    //{
    //    PlayerPrefs.SetFloat("LastADTime", PlayerPrefs.GetFloat("LastADTime", Time.time) + 60);
    //    GameManager.Instance.AppendLive();
    //    GameManager.Instance.Initialize();
    //    GUIManager.Instance.SetDefeatScreen(false);
    //}

    //public void onNonSkippableVideoClosed()
    //{
    //    GUIManager.Instance.SetActiveDefeatButtons();
    //}

    /**********************************/
}