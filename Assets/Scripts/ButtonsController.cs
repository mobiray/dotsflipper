﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GooglePlayGames;
using UnityEngine.SocialPlatforms;
using UnityEngine.UI;
using DG.Tweening;

public class ButtonsController : MonoBehaviour {

    void Start()
    {
        if(PlayerPrefs.GetInt("Sound", 1) == 0)
        {
            AudioController.MuteSound(true);
        }
        //*HERE*//
        PlayGamesPlatform.Activate();
    }

    public void OnPauseButtonClick()
    {
        GUIManager.Instance.SetPauseScreen(true);
    }

    public void OnResumeButtonClick()
    {
        GUIManager.Instance.SetPauseScreen(false);
    }

    public void OnToggleMusicClick()
    {
        if (this.GetComponent<Toggle>().isOn)
        {
            PlayerPrefs.SetInt("Sound", 1);
            AudioController.MuteSound(false);
        }
        else
        {
            PlayerPrefs.SetInt("Sound", 0);
            AudioController.MuteSound(true); 
        }
    }

    public void OnRecordsButtonClick()
    {
        Social.localUser.Authenticate((bool success) => { GPlayManager.Instance.isAuthenticate = success; });
        //Social.ShowLeaderboardUI();
        //*HERE*//
        PlayGamesPlatform.Instance.ShowLeaderboardUI("CgkIs9KWt4YHEAIQBw");
    }

    public void OnRestartButtonClick()
    {
        GUIManager.Instance.SetInactiveWatchButton();
        GameManager.Instance.RestartStage();
        GUIManager.Instance.SetPauseScreen(false);
        GUIManager.Instance.SetDefeatScreen(false);
    }

    public void OnWatchButtonClick()
    {
        GUIManager.Instance.SetInactiveRestartButton();
        AdvertismentScript.Instance.ShowVideo();
    }

    public void OnShakerButtonClick()
    {
        // GameManager.Instance.ball.transform.DOPunchPosition(Random.Range(-1f, 1f) * new Vector3(1f, 0.1f, 0f), 0.1f, 3, 0f, false);
        GameManager.Instance.ball.GetComponent<Rigidbody2D>().AddForce(new Vector2(Random.Range(-150f, 150f), -100f));
    }

    public void OnRateButtonClick()
    {
        Application.OpenURL("https://play.google.com/store/apps/details?id=com.twodtwob.relaxdots");
    }

    //public void OnAchievmentsButtonClick()
    //{
    //    Social.localUser.Authenticate((bool success) => {
    //    });
    //    Social.ShowAchievementsUI();
    //}

    public void OnNextLevelButtonClick()
    {
        GameManager.Instance.CompleteStage();
    }

    public void OnPrevLevelButtonClick()
    {
        GameManager.Instance.DecompleteStage();
    }
}
