﻿using UnityEngine;
using UnityEngine.UI;

public class GDPR : MonoBehaviour
{

    void OnEnable()
    {
        if (PlayerPrefs.HasKey("result_gdpr"))
        {
            AdvertismentScript.Instance.Init();
        }
    }

    public void OnYesClicked()
    {
        PlayerPrefs.SetInt("result_gdpr", 1);
        AdvertismentScript.Instance.Init();
        gameObject.SetActive(false);
    }

    public void OnNoClicked()
    {
        PlayerPrefs.SetInt("result_gdpr", 0);
        AdvertismentScript.Instance.Init();
        gameObject.SetActive(false);
    }

    public void OnGDPRButtonClick()
    {
        gameObject.SetActive(true);
    }

    public void OnBackButtonClick()
    {
        gameObject.SetActive(false);
    }
}