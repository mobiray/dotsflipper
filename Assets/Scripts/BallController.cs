﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class BallController : MonoBehaviour {

    public SpriteRenderer shader;

    Rigidbody2D rb;
    bool isDynamic = false;
    LevelManager lMan;

    private Color alphaC = new Color(0f, 0f, 0f, 0f);
    
   // GPlayManager gpMan;
 
    void OnEnable()
    {
        isDynamic = false;
        shader.color = new Color(0f, 0f, 0f, 0f);
    }

    void Start()
    {
        lMan = LevelManager.Instance;
      //  gpMan = GPlayManager.Instance;
        rb = this.GetComponent<Rigidbody2D>();
    }

    void OnCollisionEnter2D(Collision2D coll)
    {
        if (coll.gameObject.CompareTag("Player") && !isDynamic)
        {
            //partSystem.transform.position = (this.transform.position + coll.transform.position) / 2f;
            //partSystem.Emit(15);
            shader.color = alphaC;
            coll.gameObject.GetComponent<TrailRenderer>().endColor = coll.gameObject.GetComponent<TrailRenderer>().startColor;
            coll.gameObject.GetComponent<TrailRenderer>().startColor = this.GetComponent<SpriteRenderer>().color;
            AudioController.Play("DigitalBeep");
            lMan.IncrementCurrentScore();
            //gpMan.AppendBallsCount();
            //gpMan.AppendTouchesCount();
            rb.bodyType = RigidbodyType2D.Dynamic;
            isDynamic = true;
            GameManager.Instance.ballsToKnock--;
            if (GameManager.Instance.ballsToKnock == 0)
            {
                GameManager.Instance.CompleteStage();
            }
        }
    }

    void OnCollisionExit2D(Collision2D coll)
    {

    }
}
